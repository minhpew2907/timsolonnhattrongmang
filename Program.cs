﻿

Console.WriteLine("Nhap so luong phan tu: ");
int userInput = int.Parse(Console.ReadLine());

int[] numbers = new int[userInput];

Console.WriteLine("Nhap cac phan tu cua mang: ");

for (int i = 0; i < userInput; i++)
{
    numbers[i] = int.Parse(Console.ReadLine());
}

int largestNumber = FindLargestNumber(numbers);
Console.WriteLine($"So lon nhat trong mang la: {largestNumber}");


int FindLargestNumber(int[] number)
{
    int largeNumber = number[0];

    for (int i = 1; i < number.Length; i++)
    {
        if (number[i] > largeNumber)
        {
            largeNumber = number[i];
        }
    }
    return largeNumber;
}